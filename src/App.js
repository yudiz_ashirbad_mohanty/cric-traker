import React, { useEffect, useState } from "react";


import Cards from "./components/Cards";

import graphQlToRest from "./components/data";

function App() {
  const [Data, setCardData] = useState({});
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    graphQlToRest()
      .then((Data) =>
        setCardData(Data.data.getHomePageArticle.aResults)
      )
      .then(() => setLoading(false));
  }, []);

  return (
    <div className="app">
        {loading ? <h1>Loading...</h1> : <Cards Data={Data} />}
    </div>
  );
}

export default App;
