import React from 'react'

const Gridcard = ({Articles}) => {
    const { sTitle, dPublishDate } = Articles;
    const date =new Date(dPublishDate);
  return (
    <div>
        <h3 className='title'>{sTitle}</h3>
      <div className="date">
        <p>{date.toDateString()}</p>
      </div>
    </div>
  )
}

export default Gridcard