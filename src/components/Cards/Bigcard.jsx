import React from 'react'

const Bigcard = ({Articles}) => {
    const {sTitle,sDescription,nDuration,dPublishDate}=Articles
    const date =new Date(dPublishDate);
      
  return (
    <div>
        <h3 className='title'>{sTitle}</h3>
       <p className='des'>{sDescription}</p>
      <div className="date">
        <p>{date.toDateString()}</p>
        <p className="duration">{nDuration} Min</p>
      </div>
    </div>
  )
}

export default Bigcard