import React from "react";
import Articles from "./Articles";

function Cards({Data }) {
  console.log(Data)
  return (
    <div className="Card-main">
      {Data.map((card, key) => (
        <div className="card-sub" key={key}>
          <h2>{card.sName}</h2>
          <button>{card.sName}</button>
          <Articles Articles={card.aArticle}/>
        </div>
      ))}
    </div>
  );
}



export default Cards;
