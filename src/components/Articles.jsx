import React from 'react'
import Bigcard from './Cards/Bigcard'
import Gridcard from './Cards/Gridcard'
import Smallcard from './Cards/Smallcard'

const Articles = ({Articles}) => {
  return (
    <div className='articles'>
        <Bigcard Articles={Articles}/>
        <Smallcard Articles={Articles}/>
        <Gridcard Articles={Articles}/>
    </div>
  )
}

export default Articles